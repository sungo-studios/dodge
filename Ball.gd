extends CharacterBody2D

#Constant Variables
@export var MAX_SPEED = 2000
@export var MIN_SPEED = 400
@export var ACCELERATION = 1500 
@export var FRICTION = 1200

#Variables Declared
@onready var axis = Vector2.ZERO
var start = false
var was_holding = false
var score = 0

# Function always run
func _physics_process(delta):
	if (Input.is_action_pressed("restart")):
		position.x = 1073
		position.y = 654
		velocity.x = 0
		velocity.y = 0
		global.holding = false
		global.holding2 = false
		global.game_over = false
		
	if start == false:
		axis.x = 10
		axis.y = 10
		start = true
	
	# Runs if player is holding ball
	if global.holding:
		# Velocity of ball is 0 and follows the player around
		velocity = velocity * 0
		position.x = global.player_pos_x + 85
		position.y = global.player_pos_y - 85
		
		if (was_holding == false):
			# Possible Single-Player game
			score += 1
			#print(score)
			
		was_holding = true
		
	# Executes when the ball is released
	# Runs if player is holding ball
	if global.holding2:
		print("HES HOLDING")
		# Velocity of ball is 0 and follows the player around
		velocity = velocity * 0
		position.x = global.player2_pos_x - 85
		position.y = global.player2_pos_y + 85
		
		if (was_holding == false):
			# Possible Single-Player game
			score += 1
			#print(score)
		was_holding = true
		
	# Executes when the ball is released
	if(global.holding == false && was_holding == true && Input.is_action_just_released("catch")):
		velocity.y = MAX_SPEED * -1
		velocity.x = global.player_velocity_x * abs(global.player_axis_x) * 5
		velocity.limit_length(MAX_SPEED)
		was_holding = false
		
	if(global.holding2 == false && was_holding == true && Input.is_action_just_released("catch2")):
		velocity.y = MAX_SPEED * 1
		velocity.x = global.player_velocity_x * abs(global.player_axis_x) * 5
		velocity.limit_length(MAX_SPEED)
		was_holding = false
		
	move(delta)
	
	
# Gets Axis
func get_input_axis():
	
	if is_on_wall():
		axis.x = axis.x * -1
	if is_on_floor() || is_on_ceiling():
		axis.y = axis.y * -1
	return axis
	
# Move
func move(delta):
	
	axis = get_input_axis();

	apply_movement(axis * ACCELERATION * delta)

	move_and_slide()
	
# Movement 
func apply_movement(accel):
	if(is_on_ceiling() || is_on_floor() || is_on_wall()):
		velocity += accel*3
	if (velocity.x != 0):
		velocity.x = abs(velocity.x)/velocity.x * abs(velocity.x) - velocity.x*.02
	if (velocity.y != 0):
		velocity.y = abs(velocity.y)/velocity.y * abs(velocity.y) - velocity.y*.02
	velocity = velocity.limit_length(MAX_SPEED)
	
# Ball is caught 
func ball_caught(accel):
	velocity = accel * 0
	
	
	
