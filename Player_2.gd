extends CharacterBody2D


@export var MAX_SPEED = 500
@export var ACCELERATION = 2250
@export var FRICTION = 1200
var ball_within = false
var countdown = false
@onready var axis = Vector2.ZERO

# Function Always Runs
func _physics_process(delta):
	move(delta)
	# Established all player variabvles as global variables
	if (Input.is_action_pressed("restart")):
		position.x = 1081
		position.y = 257
	global.player2_pos_x = position.x
	global.player2_pos_y = position.y
	global.player2_axis_x = axis.x
	global.player2_axis_y = axis.y
	global.player2_velocity_x = velocity.x
	global.player2_velocity_y = velocity.y
	# Catch the ball changes global.holding to true
	if(Input.is_action_just_pressed("catch2") && ball_within == true):
		global.holding2 = true
	# Ball is thrown and global.holding to false
	if(Input.is_action_just_released("catch2")):
		global.holding2 = false

# Gets input axis
func get_input_axis():
	axis.x = int(Input.is_action_pressed("move_right2")) - int(Input.is_action_pressed("move_left2"))
	axis.y = int(Input.is_action_pressed("move_down2")) - int(Input.is_action_pressed("move_up2"))
	if (axis.y == 1 && position.y > 380):
		axis.y = 0
		velocity.y = 0
	return axis.normalized()
	
# Move function
func move(delta):
	axis = get_input_axis();
	
	if axis == Vector2.ZERO:
		apply_friction(FRICTION * delta)
		
	else: 
		apply_movement(axis * ACCELERATION * delta)
		
	move_and_slide()
	

# Friction Function
func apply_friction(amount):
	if velocity.length() > amount:
		velocity -= velocity.normalized() * amount
	else:
		velocity = Vector2.ZERO
		
# Movement Function
func apply_movement(accel):
	velocity += accel
	velocity = velocity.limit_length(MAX_SPEED)
	
# Checks to see if ball enters player area
func _on_area_2d_area_entered(area):
	#print("colided")
	ball_within = true

# Checks to see if ball exits player area
func _on_area_2d_area_exited(area):
	ball_within = false
	#print("uncolided") # Replace with function body.

# Checks to see if ball enters hitbox area and sets player out of the way to presume he is eliminated
func _on_hitbox_area_entered(area):
	if (countdown and global.holding2 == false):
		position.x = 100000
		global.game_over = true
		global.player2_score += 1
	countdown = true
