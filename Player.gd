extends CharacterBody2D


@export var MAX_SPEED = 500
@export var ACCELERATION = 2250
@export var FRICTION = 1200
var ball_within = false
var countdown = false
@onready var axis = Vector2.ZERO

# Function Always Runs
func _physics_process(delta):
	move(delta)
	# Established all player variabvles as global variables
	if (Input.is_action_pressed("restart")):
		position.x = 1073
		position.y = 833
	global.player_pos_x = position.x
	global.player_pos_y = position.y
	global.player_axis_x = axis.x
	global.player_axis_y = axis.y
	global.player_velocity_x = velocity.x
	global.player_velocity_y = velocity.y
	# Catch the ball changes global.holding to true
	if(Input.is_action_just_pressed("catch") && ball_within == true):
		global.holding = true
	# Ball is thrown and global.holding to false
	if(Input.is_action_just_released("catch")):
		global.holding = false

# Gets input axis
func get_input_axis():
	axis.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	axis.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	if (axis.y == -1 && position.y < 520):
		axis.y = 0
		velocity.y = 0
	print(position.y)
	return axis.normalized()
	
# Move function
func move(delta):
	axis = get_input_axis();
	
	if axis == Vector2.ZERO:
		apply_friction(FRICTION * delta)
		
	else: 
		apply_movement(axis * ACCELERATION * delta)
		
	move_and_slide()
	

# Friction Function
func apply_friction(amount):
	if velocity.length() > amount:
		velocity -= velocity.normalized() * amount
	else:
		velocity = Vector2.ZERO
		
# Movement Function
func apply_movement(accel):
	velocity += accel
	velocity = velocity.limit_length(MAX_SPEED)
	
# Checks to see if ball enters player area
func _on_area_2d_area_entered(area):
	#print("colided")
	ball_within = true

# Checks to see if ball exits player area
func _on_area_2d_area_exited(area):
	ball_within = false
	#print("uncolided") # Replace with function body.

# Checks to see if ball enters hitbox area and sets player out of the way to presume he is eliminated
func _on_hitbox_area_entered(area):
	if (countdown and global.holding == false):
		position.x = 100000
		global.game_over = true
		global.player_score 
	countdown = true
